<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as Application;


abstract class BaseRepository
{
    protected $with = [];
    protected $orderBy  = array('updated_at', 'desc');

    /**
     * @var Model
     */

    protected $model, $transformer;

    /**
     * @var Application
     */
    protected $app;

    /**
     * @param Application $app
     *
     * @throws \Exception
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Get searchable fields array
     *
     * @return array
     */
    abstract public function getFieldsSearchable();

    /**
     * Configure the Model
     *
     * @return string
     */
    abstract public function model();

    /**
     * Make Model instance
     *
     * @throws \Exception
     *
     * @return Model
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    /**
     * Set number of records to return
     * @return int
     */
    private function limit(){
        return (int)(request()->query('limit')) ? : 5;
    }

    /**
     * @return string
     */
    private function sortField () {
        return (string)(request()->query('sortField')) ? : 'id';
    }

    /**
     * @return string
     */
    private function sortDirection() {
        return (string)(request()->query('sortDirection')) ? : 'desc';
    }

    /**
     * @param $field
     * @return mixed
     */
    public function getSum($field)
    {
        return $this->model->sum($field);
    }

    /**
     * Count the number of specified model records in the database
     *
     * @return int
     */
    public function count()
    {
        return $this->model->count();
    }

    /**
     * @return string
     */
    private function searchFilter() {
        return (string)(request()->query('filter')) ? : '';
    }

    /**
     * @return string
     */
    private function whereField() {
        return (string)(request()->query('whereField')) ? : '';
    }

    /**
     * @return string
     */
    private function whereValue() {
        return (string)(request()->query('whereValue')) ? : '';
    }

    /**
     * @param array $load
     * @return mixed
     */
    public function getAllNoSearchPaginate($load = array()) {
        return $this->model
            ->with($load)
            ->orderBy($this->sortField(), $this->sortDirection())
            ->paginate($this->limit());
    }

    /**
     * @param array $load
     * @return mixed
     */
    public function getAll($load = array())
    {
        return $this->model->with($load)->get();
    }

    /**
     * @param array $load
     * @return mixed
     */
    public function getAllPaginate($load = array()){

        if (strlen ($this->whereField()) > 0) {
            if(strlen ($this->whereValue()) < 1) {
                return $this->model
                    ->with($load)
                    ->whereNull($this->whereField())
                    ->search($this->searchFilter(), null, true, true)
                    ->orderBy($this->sortField(), $this->sortDirection())
                    ->paginate($this->limit());
            }
            return $this->model
                ->with($load)
                ->where($this->whereField(), $this->whereValue())
                ->search($this->searchFilter(), null, true, true)
                ->orderBy($this->sortField(), $this->sortDirection())
                ->paginate($this->limit());
        }else {
            return $this->model->search($this->searchFilter(), null, true, true)
                ->with($load)
                ->orderBy($this->sortField(), $this->sortDirection())
                ->paginate($this->limit());
        }
    }

    /**
     * Paginate records for scaffold.
     *
     * @param int $perPage
     * @param array $columns
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage, $columns = ['*'], $search = [])
    {
        $query = $this->allQuery($search)->orderBy($this->sortField(), $this->sortDirection());

        return $query->paginate($perPage, $columns);
    }

    /**
     * Build a query for retrieving all records.
     *
     * @param array $search
     * @param int|null $skip
     * @param int|null $limit
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function allQuery($search = [], $skip = null, $limit = null)
    {
        $query = $this->model->newQuery();

        if (count($search)) {
            foreach($search as $key => $value) {
                if (in_array($key, $this->getFieldsSearchable())) {
                    $query->where($key, $value);
                }
            }
        }

        if (!is_null($skip)) {
            $query->skip($skip);
        }

        if (!is_null($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * Retrieve all records with given filter criteria
     *
     * @param array $search
     * @param int|null $skip
     * @param int|null $limit
     * @param array $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all($search = [], $skip = null, $limit = null, $columns = ['*'])
    {
        $query = $this->allQuery($search, $skip, $limit);

        return $query->get($columns);
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create($input)
    {
        $model = $this->model->newInstance($input);

        $model->save();

        return $model;
    }

    /**
     * Find model record for given id
     *
     * @param int $id
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function find($id, $columns = ['*'])
    {
        $query = $this->model->newQuery();

        return $query->find($id, $columns);
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     */
    public function update($input, $id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        $model->fill($input);

        $model->save();

        return $model;
    }

    /**
     * @param int $id
     *
     * @throws \Exception
     *
     * @return bool|mixed|null
     */
    public function delete($id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        return $model->delete();
    }

    /**
     * Sets relations for eager loading.
     *
     * @param $relations
     * @return $this
     */
    public function with($relations)
    {
        return $this->model->with($relations);

    }
    /**
     * @param $filters
     * @param array $pagination
     * @param array $load
     * @return mixed
     */
    public function getFiltered($filters, $pagination = array(), $load = array())
    {
        if(isset($pagination) && array_key_exists('limit', $pagination)){
            $limit = $pagination['limit'];
        }else{
            $limit = \Request::input('limit') ?: 10;
        }

        if(isset($pagination) && array_key_exists('page', $pagination)){
            $page = $pagination['page'];
        }else
            $page = 1;

        $data = $this->model->with($load)->orderBy($this->sortField(), $this->sortDirection());

        foreach ($filters as $filter) {
            $data = $this->applyFilter($filter, $data);
        }

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });
        $data = $data->paginate($limit);

        return $data;
    }

    /**
     * @param $filter
     * @param $data
     * @return mixed
     */
    private function applyFilter($filter, $data)
    {
        $whereOperators = [
            'eq'   => '=',
            'neq'  => '!=',
            'gt'   => '>',
            'gte'  => '>=',
            'lt'   => '<',
            'lte'  => '<=',
            'like' => 'LIKE',
        ];

        if (array_key_exists($filter['operator'], $whereOperators)) {
            $data = $data->where($filter['field'], $whereOperators[$filter['operator']], $filter['value']);
        }

        if ($filter['operator'] == 'in') {
            $data = $data->whereIn($filter['field'], $filter['value']);
        }

        if ($filter['operator'] == 'notin') {
            $data = $data->whereNotIn($filter['field'], $filter['value']);
        }

        if ($filter['operator'] == 'between') {
            $data = $data->whereBetween($filter['field'], $filter['value']);
        }

        if ($filter['operator'] == 'notbetween') {
            $data = $data->whereNotBetween($filter['field'], $filter['value']);
        }

        if ($filter['operator'] == 'wheredate') {
            $data = $data->whereDate($filter['field'], $filter['value']);
        }

        return $data;
    }

    /**
     * @param $filter
     * @param $data
     * @return mixed
     */

    public function findWhere($filters) {
        $data = $this->model;

        foreach ($filters as $filter) {
            $data = $this->applyFilter($filter, $data);
        }

        return $data->first();
    }
}
