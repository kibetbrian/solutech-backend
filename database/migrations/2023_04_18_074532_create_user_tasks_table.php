<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained("users")->onUpdate('cascade');
            $table->foreignId("task_id")->constrained("tasks")->onUpdate('cascade');
            $table->dateTime('due_date');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->string('remarks');
            $table->foreignId("status_id")->constrained("status")->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_tasks');
    }
};
