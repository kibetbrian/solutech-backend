<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\UserTask;
use App\Repositories\TaskRepository;
use App\Repositories\UserTaskRepository;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\CreateUserTaskAPIRequest;
use App\Http\Requests\API\UpdateUserTaskAPIRequest;

/**
 * Class UserTaskAPIController
 */
class UserTaskAPIController extends AppBaseController
{
    private  $userTaskRepository, $taskRepository;

    public function __construct(UserTaskRepository $userTaskRepo, TaskRepository $taskRepo)
    {
        $this->userTaskRepository = $userTaskRepo;
        $this->taskRepository = $taskRepo;

    }

    /**
     * Display a listing of the UserTasks.
     * GET|HEAD /user-tasks
     */
    public function index()
    {
        // $userTasks = $this->userTaskRepository
        //                                     ->with("users:id,name")
        //                                     ->with('status:id,name')
        //                                     ->with('tasks:id,name')
        //                                     ->paginate(10);

        $userTasks = UserTask::join('users', 'user_tasks.user_id', '=', 'users.id')
                                ->join('tasks', 'user_tasks.task_id', '=', 'tasks.id')
                                ->join('status', 'user_tasks.status_id', '=', 'status.id')
                                ->select('user_tasks.*', 'users.name as user_name', 'status.name as status', 'tasks.name as task_name')
                                ->paginate(10);

        return $this->sendResponse($userTasks, 'User Tasks retrieved successfully');
    }

    /**
     * Store a newly created UserTask in storage.
     * POST /user-tasks
     */
    public function store(CreateUserTaskAPIRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response([
                'status' => 'failed',
                'errors' => $request->validator->errors()
            ], 422);
        }

        $input = $request->all();
        $input['start_time'] = Carbon::parse($request->start_time)->format('Y-m-d H:i:s');
        $input['end_date'] = Carbon::parse($request->end_date)->format('Y-m-d H:i:s');
        $userTask = $this->userTaskRepository->create($input);

        /***
         * Update Task
         */
        
        $task = $this->taskRepository->find($userTask->task_id);
        $task->status_id = $userTask->status_id;
        $task->update();

        return $this->sendResponse($userTask->toArray(), 'User Task saved successfully');
    }

    /**
     * Display the specified UserTask.
     * GET|HEAD /user-tasks/{id}
     */
    public function show($id)
    {
        /** @var UserTask $userTask */
        $userTask = $this->userTaskRepository->find($id);

        if (empty($userTask)) {
            return $this->sendError('User Task not found');
        }

        return $this->sendResponse($userTask->toArray(), 'User Task retrieved successfully');
    }

    /**
     * Update the specified UserTask in storage.
     * PUT/PATCH /user-tasks/{id}
     */
    public function update($id, UpdateUserTaskAPIRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return response([
                'status' => 'failed',
                'errors' => $request->validator->errors()
            ], 422);
        }

        $input = $request->all();

        /** @var UserTask $userTask */
        $userTask = $this->userTaskRepository->find($id);

        if (empty($userTask)) {
            return $this->sendError('User Task not found');
        }

        $userTask = $this->userTaskRepository->update($input, $id);

        return $this->sendResponse($userTask->toArray(), 'UserTask updated successfully');
    }

    /**
     * Remove the specified UserTask from storage.
     * DELETE /user-tasks/{id}
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        /** @var UserTask $userTask */
        $userTask = $this->userTaskRepository->find($id);

        if (empty($userTask)) {
            return $this->sendError('User Task not found');
        }

        $userTask->delete();

        return $this->sendSuccess('User Task deleted successfully');
    }
}
