<?php

namespace App\Http\Controllers\API\Auth;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\RegisterRequest;
use App\Http\Controllers\AppBaseController;

class AuthController extends AppBaseController
{
    public function register(RegisterRequest $request) {

        if (isset($request->validator) && $request->validator->fails()) {
            return response([
                'status' => 'failed',
                'errors' => $request->validator->errors()
            ], 422);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $success['user'] =  $user;
        $success['access_token'] = $user->createToken('authToken')->plainTextToken;
        $success['token_type'] =  'Bearer';

        return $this->sendResponse($success, 'User registered successfully.');
    }

    public function login(LoginRequest $request) {
        if (isset($request->validator) && $request->validator->fails()) {
            return response([
                'status' => 'failed',
                'errors' => $request->validator->errors()
            ], 422);
        }


        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){

            $user = Auth::user();
            $success['user'] =  $user;
            $success['access_token'] = $user->createToken('authToken')->plainTextToken;
            $success['token_type'] =  'Bearer';

            return $this->sendResponse($success, 'User logged in successfully.');
        }
        else{
            return $this->sendUnauthorizedError(['Unauthorised.', ['error' => 'Unauthorised']]);
        }
    }

    public function logout() {

        $user = Auth::user();

        $user->currentAccessToken()->delete();

        return $this->sendResponse('user', 'User logged out successfully.');

    }

    public function loggedInUser () {

        $user = Auth::user();

        return $this->sendResponse($user, 'User retrieved.');
    }
}
