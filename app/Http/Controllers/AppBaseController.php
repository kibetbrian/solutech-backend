<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;

/**
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */

class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return response([
            'success' => true,
            'data' => $result,
            'message' => $message,
        ], 200);
    }

    public function sendError($error, $code = 404)
    {
        return Response::json($error, $code);
    }

    public function sendUnauthorizedError($error, $code = 401)
    {
        return Response::json($error, $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
