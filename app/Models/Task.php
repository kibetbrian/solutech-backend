<?php

namespace App\Models;

use App\Models\User;
use App\Models\UserTask;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $table = 'tasks';

    public $fillable = [
        "name",
        "description",
        "due_date",
        "status_id"
    ];

    protected $casts = [
        "name",
        "description",
        "due_date",
        "status_id"
    ];

    public static array $rules = [
        "name"          => "required|string",
        "description"   => "nullable|string",
        "due_date"      => "required|string"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function userTask()
    {
        return $this->hasOne(UserTask::class);
    }
}
