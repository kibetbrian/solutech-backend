<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public $table = 'status';

    public $fillable = [
        "name"
    ];

    public static array $rules = [
        "name" => "required|string"
    ];

   
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function userTask()
    {
        return $this->hasOne(UserTask::class);
    }
}
