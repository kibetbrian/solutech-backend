<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\TaskAPIController;
use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\StatusAPIController;
use App\Http\Controllers\API\UserTaskAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/auth/register', [AuthController::class, 'register']);

Route::post('/auth/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {

    Route::post('/auth/logout', [AuthController::class, 'logout']);

    Route::resource('status', StatusAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('tasks', TaskAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('user-tasks', UserTaskAPIController::class)
        ->except(['create', 'edit']);

    Route::post('/store/users', [UserController::class, 'store']);

    Route::post('/delete/user/{id}', [UserController::class, 'destroy']);

    Route::put('/update/user/{id}', [UserController::class, 'update']);

    Route::get('/list/all/user', [UserController::class, 'listAll']);

    Route::get('/user/{id}', [UserController::class, 'getUser']);

    Route::get('/auth/user', [UserController::class, 'authUser']);

});

