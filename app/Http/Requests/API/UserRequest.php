<?php

namespace App\Http\Requests\API;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation($validator)
    {
        $this->validator = $validator;
    }

    public function rules()
    {
        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                    break;
                }
            case 'POST':
                {
                    $rules = [
                        'name'                  => 'required',
                        'password'              => 'nullable',
                        'email'                 => 'email|required|unique:users,email,NULL,id,deleted_at,NULL'
                    ];

                    break;
                }
            case 'PUT':
            case 'PATCH':
                {
                    $rules = [
                        'name'                  => 'required',
                        'password'              => 'nullable',
                        'email'                 => ['required', Rule::unique('users')->ignore($this->user, 'id')
                            ->where(function ($query) {
                                $query->where('deleted_at', NULL);
                            })]
                    ];
                    break;
                }
            default:break;
        }
        return $rules;
    }
}
