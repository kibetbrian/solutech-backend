<?php

namespace App\Http\Controllers\API;

use App\Jobs\SendEmailJob;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\API\UserRequest;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\UpdateUserRequest;

class UserController extends AppBaseController
{
    /** @var  userRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }


    public function store(UserRequest $request) {

        if (isset($request->validator) && $request->validator->fails()) {
            return response([
                'status' => 'failed',
                'errors' => $request->validator->errors()
            ], 422);
        }

        $input = $request->all();
        $pass = $input['email'];
        $input['password'] = bcrypt($pass);

        $user = $this->userRepository->create($input); 

        return $this->sendResponse($user->toArray(), 'User saved successfully');
    }

    public function listAll() {

        $user = $this->userRepository->paginate(10);

        return $this->sendResponse($user, 'Users retrieved successfully');
    }

    public function update(UserRequest $request, $id) {

        if (isset($request->validator) && $request->validator->fails()) {
            return response([
                'status' => 'failed',
                'errors' => $request->validator->errors()
            ], 422);
        }

        $input = $request->all();
        $id = Auth::user()->id;

        if (Auth::user()->email != trim($input['email'])) {
            $rules =  array("email" => "required|email|unique:users");
            $valid = Validator::make($input['email'], $rules);
            if (count($valid->errors())) {
                return response([
                    'status' => 'failed',
                    'errors' => $valid->errors()
                ], 422);
            }
        }

        /** @var user $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('user not found');
        }

        $user = $this->userRepository->update($input, $id);

        return $this->sendResponse($user->toArray(), 'user updated successfully');
    }

    public function getUser($id) {

         /** @var User $user */
         $user = $this->userRepository->find($id);

         if (empty($user)) {
             return $this->sendError('User not found');
         }

         return $this->sendResponse($user->toArray(), 'User retrieved successfully');
    }

    public function destroy($id)
    {
        /** @var User $user */
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $user->delete();

        return $this->sendSuccess('User deleted successfully');
    }

    public function authUser() {

        $user = Auth::user();

        return $this->sendResponse($user, 'User retrieved.');
    }
}
