<?php

namespace App\Http\Requests\API;

use App\Models\UserTask;
use InfyOm\Generator\Request\APIRequest;

class CreateUserTaskAPIRequest extends APIRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation($validator)
    {
        $this->validator = $validator;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return UserTask::$rules;
    }
}
