<?php

namespace App\Models;

use App\Models\Task;
use App\Models\User;
use App\Models\Status;
use Illuminate\Database\Eloquent\Model;

class UserTask extends Model
{
    public $table = 'user_tasks';

    public $fillable = [
        "user_id",
        "task_id",
        "start_time",
        "end_time",
        "remarks",
        "status_id"
    ];

    protected $casts = [
        "user_id",
        "task_id",
        "start_time",
        "end_time",
        "remarks",
        "status_id"
    ];

    public static array $rules = [
        "user_id"       => "required",
        "task_id"       => "required",
        "start_time"    => "required",
        "end_time"      => "required",
        "remarks"       => "nullable|string",
        "status_id"     => "required",
    ];


    public function status()
    {
        return $this->belongsTo(Status::class);
    }


    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function tasks()
    {
        return $this->belongsTo(Task::class);
    }
}
